import sys

def insertionSort(arr):
    #Para ordenar cada bucket por separado.    
    for i in range(1, len(arr)):
        index = i

        while(index > 0):
            if(arr[index] < arr[index - 1]):
                tmp = arr[index - 1]
                arr[index - 1] = arr[index]
                arr[index] = tmp
                index-=1
            else:
                break

    return arr

def bucketSort(arr, maximum):
    #Ordena una secuencia de numeros usando bucket sort

    bucket = len(arr)
    max = maximum
    arrAux = [None] * (bucket + 1)

    for i in range(0,len(arr)):
        index = int(arr[i] * (bucket / max))

        print(f"{arr[i]} tiene index = {index}")
        if arrAux[index] == None:
            arrAux[index] = [arr[i]]
        else:
            arrAux[index].append(arr[i])
        
    print(f"\nLista previa antes de aplicar insertion sort y unirla:\n {arrAux}")
    arr = []
    for i in range (0, len(arrAux)):
        if arrAux[i] == None:
            continue
        elif len(arrAux[i]) > 1:
            x = insertionSort(arrAux[i])        #Se aplica insertion sort a cada bucket
            for j in x:
                arr.append(j)
        else:
            arr.append(arrAux[i][0])
    return arr



def countingSort(rango, arr):
    #Ordena una secuencia usando counting sort

    aux = [0] * (rango[1] +1)
    
    for x in range(0,len(arr)):
        aux[arr[x]]+=1                          #Revisa las frecuencias de cada elemento

    print(f"Frecuencias de los elementos: {aux}")
    
    final = []
    for i in range(rango[0],rango[1]+1):        #Une la lista dependiendo la frecuencia de cada elemento
        for j in range(0,aux[i]):
            final.append(str(i))

    return ",".join(final)


def radixSort(k, arr):
    #Ordena una secuencia usando radix sort

    diccionario = {'0' : 0, '1' : 1, '2': 2 ,
                    '3' : 3, '4' : 4, '5': 5,
                    '6' : 6, '7' : 7, '8': 8,
                    '9' : 9, 'A' : 10, 'B': 11,
                    'C' : 12, 'D' : 13, 'E': 14,
                    'F' : 15,
                    }

    for j in range(0,k):
        for i in range(1, len(arr)):
            index = i
            while(index > 0): #Obtiene el valor de cada letra y depediendo de eso cambia el lugar.
                if(diccionario[arr[index][j]] < diccionario[arr[index - 1][j]] and arr[index][:j] == arr[index-1][:j]):
                    tmp = arr[index - 1]
                    arr[index - 1] = arr[index]
                    arr[index] = tmp
                    index-=1
                else:
                    break
        
        print(f"\nEn la posicion {j}th (De izquierda a derecha)")

        for x in arr: #Imprime cada elemento cada que se termina de examinar el k digito
            print("".join(x))

    s = ""
    for x in arr:
        s+=  "".join(x)
        s+=","
    return s[:len(s)-1]
    
def main():

    try:
        f = open(str(sys.argv[2]))
        text = f.readlines()
        f.close()

        print(f"Lista sin ordenar: {text[1]}\n")

        if(str(sys.argv[1]) == 'counting' ):
            text[0] = [int(x) for x in text[0].split(",")]
            text[1] = [int(x) for x in text[1].split(",")]
            sorted = countingSort(text[0],text[1])
            print(f"\nLista ordenada: {sorted}")
        if(sys.argv[1] == 'bucket' ):
            text[0] = int(text[0])
            text[1] = [int(x) for x in text[1].split(",")]
            sorted = bucketSort(text[1],text[0])
            print(f"\nLista ordenada: {sorted}")
        if(str(sys.argv[1]) == 'radix' ):
            text[0] = int(text[0])
            text[1] = [list(x) for x in text[1].split(",")]
            sorted = radixSort(text[0], text[1])
            print(f"\nLista ordenada: {sorted}")
        
    except:
        print("Revisa los valores dados")
        
        
if __name__ == "__main__":
    main()