import sys
from graph import graph
from vertex import vertex
from edge import edge

def main():
    # Abrimos y cerramos el archivo

    f = open(str(sys.argv[1]))
    text = f.readlines()
    f.close()

    for i in range(0,len(text)):    #Quitamos los saltos de linea
        text[i] = text[i].replace("\n", "").split(",")



    x = graph()                     # Creamos la grafica

    for i in text[0]:               # Añadimos los vertices
        v = vertex(i)
        x.addV(v)

    for j in text[1:]:              # Añadimos las aristas
        e = edge(j[0],j[1])
        x.addA(e)


    cim = x.CIMaximal()             # Encontramos el conjunto independiente maximal
    print("El conjunto independiente maximal es:")
    for k in cim:
        print(k.name)

        
if __name__ == "__main__":
    main()