from vertex import vertex
from edge import edge
import math

class graph:

    def __init__(self):
        # Constructor
        self.vertices = []
        self.aristas = []

    
    def addV(self, vertex):
        # Agrega un vertice a la gráfica
        self.vertices.append(vertex)

    
    def addA(self, edge):
        # Agrega una arista a la gráfica
        self.aristas.append(edge)


    def toString(self):
        # Muestra los datos de la gráfica
        return f"vertices: {self.vertices}, aristas: {self.aristas}"

    
    def __repr__(self):
        # Muestra los datos de la gráfica
        return str(self.__dict__)

    def getVertices(self):
        s = ""
        for v in self.vertices:
            s += v.name + ","
        return s[0:len(s)-1]

    def printPowerSet(self, set,set_size): 
        # Regresa el conjunto potencia de una lista
        
        pow_set_size = (int) (math.pow(2, set_size)); 
        counter = 0; 
        j = 0; 
        
        powerset = []
        for counter in range(0, pow_set_size): 
            tmp = []
            for j in range(0, set_size): 
                if((counter & (1 << j)) > 0): 
                    tmp.append(set[j]) 
            
            powerset.append(tmp)
        
        return powerset
  

    def CIMaximal(self):
        # Regresa el conjunto independiento maximal

        powerset = self.printPowerSet(self.vertices, len(self.vertices)) # Conjunto potencia de la gráfica

        ci = []
        for i in range(0, len(powerset)):                                # Saca los vecinos de cada conjunto
            vecinos = set()
            for j in range(0, len(powerset[i])):
                for k in self.aristas:
                    if(powerset[i][j].name == k.vi):
                        vecinos.add(k.vf)
                    if(powerset[i][j].name == k.vf):
                        vecinos.add(k.vi)

            aux = False
            for x in powerset[i]:                                       # Quita los conjuntos que no son independientes
                for y in vecinos:
                    if(x.name == y):
                        aux = True
                        break
            if(aux == False):  
                ci.append([powerset[i], vecinos])                 


        max = ci[0]
        for conjunto in ci:                                              # Elige el  conjunto independiente maximal
            if(len(conjunto[0]) > len(max[0])):
                max = conjunto
                
        return max[0]

    def insertionSort(self,arr):
    
        for i in range(1, len(arr)):
            index = i

            while(index > 0):
                if(arr[index][2] < arr[index - 1][2]):
                    tmp = arr[index - 1]
                    arr[index - 1] = arr[index]
                    arr[index] = tmp
                    index-=1
                else:
                    break

        return arr

    
    def Prim(self):


        vertices = [[i, "-", "inf", False, False] for i in self.vertices ]
        Q = [vertices[0]]
        vertices[0][2] = '0'
        vecinos = []

        tmp = []
        
        for i in range(0,len(vertices)):
            tmp.append(Q[0][0].name)
            for e in self.aristas:
                if(e.vi == Q[0][0].name):
                    vecinos.append([e.vf,e.w])
                if(e.vf == Q[0][0].name):
                    vecinos.append([e.vi,e.w])


            for i in vecinos:
                for j in vertices:
                    if(j[0].name == i[0]):
                        if(j[1] == "-"):
                            j[1] = Q[0][0].name
                        if(j[2] == "inf"):
                            j[2] = i[1]
                            j[1] = Q[0][0].name
                        if(int(i[1]) < int(j[2])):
                            for t in tmp:
                                if(t == Q[0][0].name):
                                    j[2] = i[1]
                                    j[1] = Q[0][0].name
                                if(t == i[0]):
                                    break
                        j[3] = True
                        break
                        
            for i in vertices:
                if(i[0].name == Q[0][0].name):
                    i[4] = True

            Q = []
            for k in vertices:
                if(k[4] == False and k[1] != "-"):
                    Q.append(k)

            Q = self.insertionSort(Q)

        return vertices