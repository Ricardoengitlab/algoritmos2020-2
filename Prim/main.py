import sys
from graph import graph
from vertex import vertex
from edge import edge

def main():
    # Abrimos y cerramos el archivo

    f = open(str(sys.argv[1]))
    text = f.readlines()
    f.close()

    for i in range(0,len(text)):    #Quitamos los saltos de linea
        text[i] = text[i].replace("\n", "").split(",")



    x = graph()                     # Creamos la grafica

    for i in text[0]:               # Añadimos los vertices
        v = vertex(i)
        x.addV(v)

    for j in text[1:]:              # Añadimos las aristas
        e = edge(j[0],j[1],j[2])
        x.addA(e)


    prim = x.Prim()

    p = open("prim.txt", "w")
    p.write(x.getVertices())
    for e in prim:
        print(e)
        if(e[2] != "0"):
            p.write(f"\n{e[0].name},{e[1]},{e[2]}")

        
if __name__ == "__main__":
    main()