import sys


def BFS(vertices, nodo, aristas):
    #Usa BFS para marcar todos los vertices de la componente a la que pertenece dicho nodo

    stack = [nodo]
    v = []
    e = set()

    while(len(stack) > 0):
        aux = False
        for i in range(0, len(aristas)):                                            # Mientras existan aristas revisamos cada una para hacer el marcado
            if(aristas[i][0] == stack[0] and vertices.get(aristas[i][1]) == False): #Si esta el vertice no está marcado y está en el lado izquierdo (X, y)
                vertices[aristas[i][1]] = True
                v.append(aristas[i][1])
                e.add("-".join(aristas[i]))
                stack.append(aristas[i][1])
                aux = True
            if(aristas[i][1] == stack[0] and vertices.get(aristas[i][0]) == False): #Si esta el vertice no está marcado y está en el lado izquierdo (x, Y)
                vertices[aristas[i][0]] = True
                v.append(aristas[i][0])
                e.add("-".join(aristas[i]))
                stack.append(aristas[i][0])
                aux = True

        #print(stack)
        if(aux == False):                   # Cuando ya no encuentra más vertices por marcar se saca del stack el vertice buscado y continua con el siguiente
            stack = stack[1:]

    if(v == []):                   # En caso de que sea un vértice aislado y lo muestra
        return [nodo, []]
    else:                                   # En caso de que sea una componente con más de un vértice y lo muestra
        return [[",".join(v)],e]                 


def DFS(vertices,nodo, aristas):
    #Usa DFS para marcar todos los vertices de la componente a la que pertenece dicho nodo

    stack = [nodo]
    v = []
    e = set()

    while(len(stack) > 0):
        aux = False
        for i in range(0, len(aristas)):                                            # Mientras existan aristas revisamos cada una para hacer el marcado
            if(aristas[i][0] == stack[0] and vertices.get(aristas[i][1]) == False): #Si esta el vertice no está marcado y está en el lado izquierdo (X, y)
                vertices[aristas[i][1]] = True
                stack.insert(0, aristas[i][1])
                aux = True
                v.append(aristas[i][1])
                e.add("-".join(aristas[i]))
                break
            if(aristas[i][1] == stack[0] and vertices.get(aristas[i][0]) == False): #Si esta el vertice no está marcado y está en el lado izquierdo (x, Y)
                vertices[aristas[i][0]] = True
                stack.insert(0, aristas[i][0])
                aux = True
                v.append(aristas[i][0])
                e.add("-".join(aristas[i]))
                break
                
        #print(stack)
        if(aux == False):                   # Cuando ya no encuentra más vertices por marcar se saca del stack el vertice buscado y continua con el siguiente
            stack = stack[1:]

    if(v == []):                   # En caso de que sea un vértice aislado y lo muestra
        return [nodo, []]
    else:                                   # En caso de que sea una componente con más de un vértice y lo muestra
        return [[",".join(v)],e]



def main():

    #Abrimos y cerramos el archivo
    f = open(str(sys.argv[1]))
    text = f.readlines()
    f.close()

    for i in range(0,len(text)): #Quitamos los saltos de linea
        text[i] = text[i].replace("\n", "").split(",")

    #Creamos nuestro registro de vertices marcados
    vertices = { i : False for i in text[0] }

    componentes = 0
    for i in text[0]:
        #Se ejecuta el programa a petición del usuario
        if vertices.get(i) == False:
            componentes+=1
            if(sys.argv[2] == "BFS"):
                bfs = BFS(vertices, i, text[1:])
                print(f"Componente #{componentes} son los vertices {bfs[0]} y aristas {bfs[1]}")
            if(sys.argv[2] == "DFS"):
                dfs = DFS(vertices, i, text[1:])
                print(f"Componente #{componentes} son los vertices {dfs[0]} y aristas {dfs[1]}")


    print(f"El número de componentes es = {componentes}") #Muestra el total de componentes
        
if __name__ == "__main__":
    main()